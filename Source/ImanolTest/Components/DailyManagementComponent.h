#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ImanolTest/DataAssets/DA_States.h"
#include "DailyManagementComponent.generated.h"

class AAIGuyCharacter;
class AAIGuyController;

UCLASS(BlueprintType, Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class IMANOLTEST_API UDailyManagementComponent : public UActorComponent
{
	GENERATED_BODY()

protected:
	UPROPERTY(BlueprintReadOnly)
	AAIGuyCharacter* AIGuyOwner = nullptr;

	UPROPERTY()
	AAIGuyController* AIGuyController = nullptr;
	
	/** Visited ActivitySpots of the day. Each day it resets. */
	UPROPERTY()
	TArray<AActivitySpot*> VisitedSpotsPerDay;
	UPROPERTY()
	AActivitySpot* CurrentActivitySpot = nullptr;

	UPROPERTY(EditAnywhere, Category = "AI Guys State", meta = (AllowPrivateAccess = true))
	TSubclassOf<UDA_States> DA_States;

	UPROPERTY()
	UDA_States* AvailableStates;
	
	/** If the state you are choosing is not contained in the selected DA_States, then the first available option
	 * of DA_States will be chosen. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AI Guys State", meta = (EditCondition = "DA_States != nullptr" , AllowPrivateAccess = true))
	EStates CurrentState = EStates::None;

	UPROPERTY()
	float AccumulatedStateTime = 0.f;

public:	
	UDailyManagementComponent();

	/** Returns true if successfully finds and assigns an ActivitySpot. */
	UFUNCTION(BlueprintCallable)
	bool AssignRandomActivitySpot(bool chooseSleepingSpot);

	/** Returns if the AI still has time left in the current state. */
	UFUNCTION(BlueprintCallable)
	bool CheckHasCurrentStateTimeLeft();

	UFUNCTION(BlueprintCallable)
	float GetCurrentStateTimeLeft();

	UFUNCTION(BlueprintCallable)
	void AddTimeOnCurrentState(float timeToAdd);

	UFUNCTION(BlueprintCallable)
	void ResetCurrentStateTime();

	UFUNCTION(BlueprintCallable)
	void ResetVisitedSpots();

#pragma region GettersAndSetters
	UFUNCTION(BlueprintCallable)
	AAIGuyCharacter* GetAIGuyOwner();

	UFUNCTION(BlueprintCallable)
	TArray<AActivitySpot*>& GetVisitedSpots();

	UFUNCTION(BlueprintCallable)
	EStates GetCurrentState();
	UFUNCTION(BlueprintCallable)
	void SetCurrentState(EStates newState);
#pragma endregion

	void ResetCurrentSpotAndBT();

	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;

protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	void SetAvailableStates();
};
