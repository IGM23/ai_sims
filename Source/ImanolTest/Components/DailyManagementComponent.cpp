#include "DailyManagementComponent.h"

#include "ImanolTest/Activities/ActivitySpot.h"
#include "ImanolTest/Characters/AIGuyCharacter.h"
#include "ImanolTest/Controllers/AIGuyController.h"
#include "ImanolTest/Managers/AIGuysManager.h"

UDailyManagementComponent::UDailyManagementComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UDailyManagementComponent::BeginPlay()
{
	Super::BeginPlay();

	AIGuyOwner = Cast<AAIGuyCharacter>(GetOwner());
	if(AIGuyOwner)
		AIGuyController = Cast<AAIGuyController>(AIGuyOwner->GetController());
}

void UDailyManagementComponent::SetAvailableStates()
{
	UDA_States* states = nullptr;
	CurrentState = EStates::None;
	
	if(DA_States->IsValidLowLevel() && DA_States->GetDefaultObject<UDA_States>())
	{
		states = DA_States->GetDefaultObject<UDA_States>();
		CurrentState = states->GetAllStates()[0];
	}
	
	AvailableStates = states;
}

bool UDailyManagementComponent::AssignRandomActivitySpot(bool chooseSleepingSpot)
{
	UAIGuysManager* aiGuysManager = GetWorld()->GetSubsystem<UAIGuysManager>();
	if(aiGuysManager == nullptr)
		return false;

	CurrentActivitySpot = aiGuysManager->ObtainRandomActivitySpot(chooseSleepingSpot, VisitedSpotsPerDay);
	if(CurrentActivitySpot)
	{
		CurrentActivitySpot->SetIsSpotOccupied(true);
		CurrentActivitySpot->SetAIGuyInThisSpot(AIGuyOwner);
		VisitedSpotsPerDay.Add(CurrentActivitySpot);

		if(AIGuyController)
			AIGuyController->SetActivitySpotAndLocationInBB(CurrentActivitySpot);

		return true;
	}
	
	ResetCurrentSpotAndBT();
	return false;
}

bool UDailyManagementComponent::CheckHasCurrentStateTimeLeft()
{
	const float limitTime = AvailableStates->GetDurationOfState(CurrentState);

	return AccumulatedStateTime < limitTime;
}

float UDailyManagementComponent::GetCurrentStateTimeLeft()
{
	float remainingTime = 0.f;
	if(CheckHasCurrentStateTimeLeft())
		remainingTime = AvailableStates->GetDurationOfState(CurrentState) - AccumulatedStateTime;
			
	return remainingTime;
}

void UDailyManagementComponent::AddTimeOnCurrentState(float timeToAdd)
{
	AccumulatedStateTime += timeToAdd;
}

void UDailyManagementComponent::ResetCurrentStateTime()
{
	AccumulatedStateTime = 0.f;
}

void UDailyManagementComponent::ResetVisitedSpots()
{
	VisitedSpotsPerDay.Empty();
}

#pragma region GettersAndSetters
AAIGuyCharacter* UDailyManagementComponent::GetAIGuyOwner()
{
	return AIGuyOwner;
}

TArray<AActivitySpot*>& UDailyManagementComponent::GetVisitedSpots()
{
	return VisitedSpotsPerDay;
}

EStates UDailyManagementComponent::GetCurrentState()
{
	return CurrentState;
}

void UDailyManagementComponent::SetCurrentState(EStates newState)
{
	CurrentState = newState;
}
#pragma endregion

void UDailyManagementComponent::ResetCurrentSpotAndBT()
{
	if(CurrentActivitySpot)
	{
		CurrentActivitySpot->ExitCurrentActivitySpot();
	}
	
	CurrentActivitySpot = nullptr;

	if(AIGuyController)
		AIGuyController->ClearActivitySpotAndLocationInBB();
}

void UDailyManagementComponent::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	FName propertyName = (PropertyChangedEvent.Property != nullptr) ? PropertyChangedEvent.Property->GetFName() : NAME_None;

	if(propertyName == FName("DA_States"))
		SetAvailableStates();
	else if(propertyName == FName("CurrentState") && AvailableStates && !AvailableStates->GetAllStates().Contains(CurrentState))
		CurrentState = AvailableStates->GetAllStates()[0];
}
