#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ActivitySpot.generated.h"

class AAIGuyCharacter;
class ATargetPoint;
class UActivity;

UCLASS(Abstract, Blueprintable, BlueprintType)
class IMANOLTEST_API AActivitySpot : public AActor
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = true))
	bool IsSleepingSpot = false;

	/** Locations of interest belonging to this Spot that can be used for some Activities. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	TArray<ATargetPoint*> TargetPoints;
	
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true))
	TArray<TSubclassOf<UActivity>> ActivitiesInThisSpot;
	
	UPROPERTY()
	TArray<UActivity*> AllActivities;

	UPROPERTY()
	TArray<UActivity*> CompletedActivities;

	UPROPERTY()
	UActivity* CurrentActivity = nullptr;

	UPROPERTY()
	bool IsSpotOccupied = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	AAIGuyCharacter* AIGuyInThisSpot;
	
public:	
	AActivitySpot();

	virtual void Tick(float DeltaSeconds) override;

	/** Gets a random Activity which has not been done yet. */
	UActivity* GetRandomActivity();
	
	void ChooseAndStartActivity();
	
	bool IsActivitySpotCompleted() const;

	void ExitCurrentActivitySpot();

#pragma region GettersAndSetters
	UFUNCTION(BlueprintCallable)
	bool GetIsSleepingSpot();
	UFUNCTION(BlueprintCallable)
	bool GetIsSpotOccupied();
	UFUNCTION(BlueprintCallable)
	void SetIsSpotOccupied(bool isOccupied);
	UFUNCTION(BlueprintCallable)
	TArray<UActivity*>& GetActivities();
	UFUNCTION(BlueprintCallable)
	AAIGuyCharacter* GetAIGuyInThisSpot();
	UFUNCTION(BlueprintCallable)
	void SetAIGuyInThisSpot(AAIGuyCharacter* aiGuyCharacter);
#pragma endregion

protected:
	virtual void BeginPlay() override;

	void ManageActivityDelegates(bool bindDelegates, UActivity* activity);

	UFUNCTION(BlueprintCallable)
	void OnActivityStarted(UActivity* activityStarted);

	UFUNCTION(BlueprintCallable)
	void OnActivityFinished(UActivity* activityFinished);

	UFUNCTION(BlueprintCallable)
	void OnActivityAborted(UActivity* activityAborted);

	void LeaveActivitySpot();

};
