#include "ActivitySpot.h"

#include "Activity.h"

AActivitySpot::AActivitySpot()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AActivitySpot::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if(CurrentActivity)
	{
		if(CurrentActivity->CheckActivityEndCondition())
			CurrentActivity->EndActivity();
		else
			CurrentActivity->DoingActivity(DeltaSeconds);
	}
}

UActivity* AActivitySpot::GetRandomActivity()
{
	int32 indexChecked = 0;
	for(int32 randomIndex = FMath::RandRange(0, AllActivities.Num() - 1); indexChecked < AllActivities.Num(); ++randomIndex)
	{
		if(randomIndex == AllActivities.Num())
			randomIndex = 0;

		UActivity* currentActivity = AllActivities[randomIndex];
		if(currentActivity && !CompletedActivities.Contains(currentActivity))
			return currentActivity;

		++indexChecked;
	}

	return nullptr;
}

void AActivitySpot::ChooseAndStartActivity()
{
	CurrentActivity = GetRandomActivity();
	if(CurrentActivity)
	{
		ManageActivityDelegates(true, CurrentActivity);
		CurrentActivity->SetActivitySpot(this);
		CurrentActivity->StartActivity(AIGuyInThisSpot);
	}
}

bool AActivitySpot::IsActivitySpotCompleted() const
{
	return CompletedActivities.Num() == AllActivities.Num();
}

void AActivitySpot::ExitCurrentActivitySpot()
{
	if(CurrentActivity)
		CurrentActivity->AbortActivity();
	else
		OnActivityAborted(nullptr);
}

#pragma region GettersAndSetters
bool AActivitySpot::GetIsSleepingSpot()
{
	return IsSleepingSpot;
}

bool AActivitySpot::GetIsSpotOccupied()
{
	return IsSpotOccupied;
}

void AActivitySpot::SetIsSpotOccupied(bool isOccupied)
{
	IsSpotOccupied = isOccupied;
}

TArray<UActivity*>& AActivitySpot::GetActivities()
{
	return AllActivities;
}

AAIGuyCharacter* AActivitySpot::GetAIGuyInThisSpot()
{
	return AIGuyInThisSpot;
}

void AActivitySpot::SetAIGuyInThisSpot(AAIGuyCharacter* aiGuyCharacter)
{
	AIGuyInThisSpot = aiGuyCharacter;
}
#pragma endregion

void AActivitySpot::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickEnabled(false);

	for (int32 i = 0; i < ActivitiesInThisSpot.Num(); ++i)
	{
		UClass* childActivityClass = ActivitiesInThisSpot[i];
		UActivity* activity = NewObject<UActivity>(this, childActivityClass);
		if(activity)
		{
			AllActivities.Add(activity);
			activity->SetActivitySpot(this);
		}
	}
}

void AActivitySpot::ManageActivityDelegates(bool bindDelegates, UActivity* activity)
{
	if(AIGuyInThisSpot == nullptr || activity == nullptr)
		return;
	
	if(bindDelegates)
	{
		activity->ActivityStartedDelegate.AddDynamic(this, &AActivitySpot::OnActivityStarted);
		activity->ActivityFinishedDelegate.AddDynamic(this, &AActivitySpot::OnActivityFinished);
		activity->ActivityAbortedDelegate.AddDynamic(this, &AActivitySpot::OnActivityAborted);
	}
	else
	{
		activity->ActivityStartedDelegate.RemoveDynamic(this, &AActivitySpot::OnActivityStarted);
		activity->ActivityFinishedDelegate.RemoveDynamic(this, &AActivitySpot::OnActivityFinished);
		activity->ActivityAbortedDelegate.RemoveDynamic(this, &AActivitySpot::OnActivityAborted);
	}
}

void AActivitySpot::OnActivityStarted(UActivity* activityStarted)
{
	SetActorTickEnabled(true);
}

void AActivitySpot::OnActivityFinished(UActivity* activityFinished)
{
	CompletedActivities.Add(activityFinished);
	CurrentActivity = nullptr;
	SetActorTickEnabled(false);
	ChooseAndStartActivity();
}

void AActivitySpot::OnActivityAborted(UActivity* activityAborted)
{
	SetActorTickEnabled(false);
	LeaveActivitySpot();
}

void AActivitySpot::LeaveActivitySpot()
{
	if(CurrentActivity)
	{
		ManageActivityDelegates(false, CurrentActivity);
		CurrentActivity = nullptr;
	}
	
	for(UActivity* activity : CompletedActivities)
		ManageActivityDelegates(false, activity);
	
	CompletedActivities.Empty();
	IsSpotOccupied = false;
	AIGuyInThisSpot = nullptr;
}