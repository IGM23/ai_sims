#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Activity.generated.h"

class AActivitySpot;
class AAIGuyCharacter;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FActivityStarted, UActivity*, activityStarted);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FActivityFinished, UActivity*, activityFinished);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FActivityAborted, UActivity*, activityAborted);

UCLASS(Blueprintable, BlueprintType)
class IMANOLTEST_API UActivity : public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable)
	FActivityStarted ActivityStartedDelegate;
	UPROPERTY(BlueprintAssignable)
	FActivityFinished ActivityFinishedDelegate;
	UPROPERTY(BlueprintAssignable)
	FActivityAborted ActivityAbortedDelegate;
	
protected:
	/** When the Activity starts, the AIGuy's location will be set like the one of the ActivitySpot. */
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true))
	bool CopyActivitySpotLocationStart = false;

	/** When the Activity ends, the AIGuy's location will be set like the one of the ActivitySpot. */
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true))
	bool CopyActivitySpotLocationEnd = false;

	/** When the Activity starts, the AIGuy's rotation will be set like the one of the ActivitySpot. */
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true))
	bool CopyActivitySpotRotationStart = true;

	/** When the Activity ends, the AIGuy's rotation will be set like the one of the ActivitySpot. */
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true))
	bool CopyActivitySpotRotationEnd = false;

	/** By default, the condition to end this Activity will be to reach this time doing it.
	 * If CheckActivityEndCondition is overriden, we can ignore this parameter. */
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true))
	float TimeToComplete = 30.f;

	UPROPERTY()
	float TimeDoingActivity = 0.f;

	UPROPERTY(BlueprintReadOnly)
	AAIGuyCharacter* AIGuyCharacter = nullptr;

	UPROPERTY(BlueprintReadOnly)
	AActivitySpot* ActivitySpot = nullptr;
	
public:
	UActivity(){}

	/** Setup in this function all the things you want to do before starting the Activity.
	 * Make sure parent function is always called. */
	UFUNCTION(BlueprintNativeEvent)
	void StartActivity(AAIGuyCharacter* AIGuyCharacterInstigator);

	/** The Activity itself.
	* Make sure parent function is always called. */
	UFUNCTION(BlueprintNativeEvent)
	void DoingActivity(float deltaSeconds);

	/** Called when the Activity finishes correctly.
	 * Make sure parent function is always called. */
	UFUNCTION(BlueprintNativeEvent)
	void EndActivity();

	/** Called when the Activity finishes not in the successful way.
	* Make sure parent function is always called. */
	UFUNCTION(BlueprintNativeEvent)
	void AbortActivity();

	/** Checks the conditions to finish the Activity */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	bool CheckActivityEndCondition();
	
	UFUNCTION(BlueprintCallable)
	float GetTimeDoingActivity() const;

	UFUNCTION(BlueprintCallable)
	AActivitySpot* GetActivitySpot();
	UFUNCTION(BlueprintCallable)
	void SetActivitySpot(AActivitySpot* spot);

protected:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void ResetActivity();
	
};
