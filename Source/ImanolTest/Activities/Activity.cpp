#include "Activity.h"

#include "ActivitySpot.h"
#include "ImanolTest/Characters/AIGuyCharacter.h"

void UActivity::StartActivity_Implementation(AAIGuyCharacter* AIGuyCharacterInstigator)
{
	AIGuyCharacter = AIGuyCharacterInstigator;
	if(ActivitySpot)
	{
		if(CopyActivitySpotLocationStart)
			AIGuyCharacter->SetActorLocation(ActivitySpot->GetActorLocation());
		if(CopyActivitySpotRotationStart)
			AIGuyCharacter->SetActorRotation(ActivitySpot->GetActorRotation());
	}
	
	ActivityStartedDelegate.Broadcast(this);
}

void UActivity::DoingActivity_Implementation(float deltaSeconds)
{
	TimeDoingActivity += deltaSeconds;
}

void UActivity::EndActivity_Implementation()
{
	ResetActivity();
	ActivityFinishedDelegate.Broadcast(this);
}

void UActivity::AbortActivity_Implementation()
{
	ResetActivity();
	ActivityAbortedDelegate.Broadcast(this);
}

bool UActivity::CheckActivityEndCondition_Implementation()
{
	return TimeDoingActivity >= TimeToComplete;
}

float UActivity::GetTimeDoingActivity() const
{
	return TimeDoingActivity;
}

AActivitySpot* UActivity::GetActivitySpot()
{
	return ActivitySpot;
}

void UActivity::SetActivitySpot(AActivitySpot* spot)
{
	ActivitySpot = spot;
}

void UActivity::ResetActivity_Implementation()
{
	if(AIGuyCharacter && ActivitySpot)
	{
		if(CopyActivitySpotLocationEnd)
			AIGuyCharacter->SetActorLocation(ActivitySpot->GetActorLocation());
		if(CopyActivitySpotRotationEnd)
			AIGuyCharacter->SetActorRotation(ActivitySpot->GetActorRotation());
	}
	
	TimeDoingActivity = 0.f;
	AIGuyCharacter = nullptr;
}
