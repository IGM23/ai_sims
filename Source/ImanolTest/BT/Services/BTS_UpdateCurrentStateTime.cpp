#include "BTS_UpdateCurrentStateTime.h"

#include "ImanolTest/Characters/AIGuyCharacter.h"
#include "ImanolTest/Components/DailyManagementComponent.h"
#include "ImanolTest/Controllers/AIGuyController.h"

UBTS_UpdateCurrentStateTime::UBTS_UpdateCurrentStateTime()
{
	bNotifyTick = true;
	NodeName = "Update Current State Time";
}

void UBTS_UpdateCurrentStateTime::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	AAIGuyController* aiGuyController = Cast<AAIGuyController>(OwnerComp.GetAIOwner());
	if(aiGuyController)
	{
		AAIGuyCharacter* aiGuyCharacter = aiGuyController->GetControlledAIGuy();
		if(aiGuyCharacter)
		{
			if(aiGuyCharacter->GetDailyManagementComponent())
				aiGuyCharacter->GetDailyManagementComponent()->AddTimeOnCurrentState(DeltaSeconds);
		}
	}
}

