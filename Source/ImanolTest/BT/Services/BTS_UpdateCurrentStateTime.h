#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "BTS_UpdateCurrentStateTime.generated.h"

UCLASS()
class IMANOLTEST_API UBTS_UpdateCurrentStateTime : public UBTService
{
	GENERATED_BODY()

public:
	UBTS_UpdateCurrentStateTime();

protected:
	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
	
};
