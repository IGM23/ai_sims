#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTDecorator.h"
#include "BTD_CheckIfStateHasEnded.generated.h"

UCLASS()
class IMANOLTEST_API UBTD_CheckIfStateHasEnded : public UBTDecorator
{
	GENERATED_BODY()

public:
	UBTD_CheckIfStateHasEnded();

protected:
	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

	bool HasToChangeState(UBehaviorTreeComponent* OwnerComp) const;
	
};
