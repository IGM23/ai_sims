#include "BTD_CheckIfStateHasEnded.h"

#include "ImanolTest/Characters/AIGuyCharacter.h"
#include "ImanolTest/Components/DailyManagementComponent.h"
#include "ImanolTest/Controllers/AIGuyController.h"

UBTD_CheckIfStateHasEnded::UBTD_CheckIfStateHasEnded()
{
	bNotifyTick = true;
	NodeName = "Check if current state ended";
}

bool UBTD_CheckIfStateHasEnded::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	return HasToChangeState(&OwnerComp);
}

void UBTD_CheckIfStateHasEnded::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	if(IsInversed() == HasToChangeState(&OwnerComp))
		OwnerComp.RequestExecution(this);

}

bool UBTD_CheckIfStateHasEnded::HasToChangeState(UBehaviorTreeComponent* OwnerComp) const
{
	AAIGuyController* aiGuyController = Cast<AAIGuyController>(OwnerComp->GetAIOwner());
	if(aiGuyController)
	{
		AAIGuyCharacter* aiGuyCharacter = aiGuyController->GetControlledAIGuy();
		if(aiGuyCharacter)
		{
			if(aiGuyCharacter->GetDailyManagementComponent() && !aiGuyCharacter->GetDailyManagementComponent()->CheckHasCurrentStateTimeLeft())
				return true;
		}
	}

	return false;
}
