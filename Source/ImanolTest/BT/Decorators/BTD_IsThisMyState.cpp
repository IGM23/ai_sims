#include "BTD_IsThisMyState.h"

#include "ImanolTest/Characters/AIGuyCharacter.h"
#include "ImanolTest/Controllers/AIGuyController.h"

bool UBTD_IsThisMyState::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	AAIGuyController* aiGuyController = Cast<AAIGuyController>(OwnerComp.GetAIOwner());
	if(aiGuyController)
	{
		AAIGuyCharacter* aiGuyCharacter = aiGuyController->GetControlledAIGuy();
		if(aiGuyCharacter)
		{
			if(aiGuyCharacter->GetDailyManagementComponent() && aiGuyCharacter->GetDailyManagementComponent()->GetCurrentState() == StateToCheck)
				return true;
		}
	}
	
	return false;
}
