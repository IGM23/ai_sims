#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTDecorator.h"
#include "ImanolTest/Components/DailyManagementComponent.h"
#include "BTD_IsThisMyState.generated.h"

UCLASS()
class IMANOLTEST_API UBTD_IsThisMyState : public UBTDecorator
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = true))
	EStates StateToCheck = EStates::None;
	
	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;
};
