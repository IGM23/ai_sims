#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "ImanolTest/Components/DailyManagementComponent.h"
#include "BTT_ChangeToState.generated.h"

UCLASS()
class IMANOLTEST_API UBTT_ChangeToState : public UBTTaskNode
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = true))
	EStates NewState = EStates::None;

public:
	UBTT_ChangeToState();
	
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
