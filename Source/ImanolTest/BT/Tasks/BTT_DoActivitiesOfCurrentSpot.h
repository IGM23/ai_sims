#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTT_DoActivitiesOfCurrentSpot.generated.h"

class AActivitySpot;
class AAIGuyController;

UCLASS()
class IMANOLTEST_API UBTT_DoActivitiesOfCurrentSpot : public UBTTaskNode
{
	GENERATED_BODY()

	UPROPERTY()
	AActivitySpot* ChosenActivitySpot = nullptr;

	UPROPERTY()
	AAIGuyController* AIGuyController = nullptr;
	
public:
	UBTT_DoActivitiesOfCurrentSpot();
	
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	virtual void TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
	virtual void OnTaskFinished(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, EBTNodeResult::Type TaskResult) override;
	
};
