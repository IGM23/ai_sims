#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTT_ChooseRandomSpot.generated.h"

class AAIGuyController;
class UActivity;

/* The AI will choose a random available (not occupied) ActivitySpot
 * and store it on the Blackboard.
 */

UCLASS()
class IMANOLTEST_API UBTT_ChooseRandomSpot : public UBTTaskNode
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = true))
	bool ChooseSleepingSpot = false;

public:
	UBTT_ChooseRandomSpot();
		
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	
};
