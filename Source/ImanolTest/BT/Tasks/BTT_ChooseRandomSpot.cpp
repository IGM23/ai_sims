#include "BTT_ChooseRandomSpot.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "ImanolTest/Characters/AIGuyCharacter.h"
#include "ImanolTest/Components/DailyManagementComponent.h"
#include "ImanolTest/Controllers/AIGuyController.h"

UBTT_ChooseRandomSpot::UBTT_ChooseRandomSpot()
{
	NodeName = "Choose Random Activity Spot";
}

EBTNodeResult::Type UBTT_ChooseRandomSpot::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AAIGuyController* aiGuyController = Cast<AAIGuyController>(OwnerComp.GetAIOwner());
	if(aiGuyController)
	{
		AAIGuyCharacter* aiGuyCharacter = aiGuyController->GetControlledAIGuy();
		if(aiGuyCharacter)
		{
			if(aiGuyCharacter->GetDailyManagementComponent() && aiGuyCharacter->GetDailyManagementComponent()->AssignRandomActivitySpot(ChooseSleepingSpot))
				return EBTNodeResult::Succeeded;
		}
	}
	
	return EBTNodeResult::Failed;
}