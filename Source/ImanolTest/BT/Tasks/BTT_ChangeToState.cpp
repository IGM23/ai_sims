#include "BTT_ChangeToState.h"

#include "ImanolTest/Characters/AIGuyCharacter.h"
#include "ImanolTest/Controllers/AIGuyController.h"

UBTT_ChangeToState::UBTT_ChangeToState()
{
	NodeName = "Change To State";
}

EBTNodeResult::Type UBTT_ChangeToState::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AAIGuyController* aiGuyController = Cast<AAIGuyController>(OwnerComp.GetAIOwner());
	if(aiGuyController)
	{
		AAIGuyCharacter* aiGuyCharacter = aiGuyController->GetControlledAIGuy();
		if(aiGuyCharacter)
		{
			if(aiGuyCharacter->GetDailyManagementComponent())
			{
				aiGuyCharacter->GetDailyManagementComponent()->SetCurrentState(NewState);
				aiGuyCharacter->GetDailyManagementComponent()->ResetCurrentStateTime();
				if(NewState == EStates::Awake)
					aiGuyCharacter->GetDailyManagementComponent()->ResetVisitedSpots();

				return EBTNodeResult::Succeeded;
			}
		}
	}

	return EBTNodeResult::Failed;
}

