#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTT_AbortActivityInCurrentSpot.generated.h"

UCLASS()
class IMANOLTEST_API UBTT_AbortActivityInCurrentSpot : public UBTTaskNode
{
	GENERATED_BODY()

public:
	UBTT_AbortActivityInCurrentSpot();
	
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	
};
