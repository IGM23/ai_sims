#include "BTT_AbortActivityInCurrentSpot.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "ImanolTest/Activities/ActivitySpot.h"
#include "ImanolTest/Characters/AIGuyCharacter.h"
#include "ImanolTest/Components/DailyManagementComponent.h"
#include "ImanolTest/Controllers/AIGuyController.h"

UBTT_AbortActivityInCurrentSpot::UBTT_AbortActivityInCurrentSpot()
{
	NodeName = "Abort current Activity";
}

EBTNodeResult::Type UBTT_AbortActivityInCurrentSpot::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AAIGuyController* aiGuyController = Cast<AAIGuyController>(OwnerComp.GetAIOwner());
	if(aiGuyController)
	{
		AActivitySpot* currentActivitySpot = aiGuyController->GetActivitySpotFromBB();
		if(currentActivitySpot)
		{
			currentActivitySpot->ExitCurrentActivitySpot();

			AAIGuyCharacter* aiGuyCharacter = aiGuyController->GetControlledAIGuy();
			if(aiGuyCharacter && aiGuyCharacter->GetDailyManagementComponent())
				aiGuyCharacter->GetDailyManagementComponent()->ResetCurrentSpotAndBT();

			return EBTNodeResult::Succeeded;
		}
	}
	
	return EBTNodeResult::Failed;
}
