#include "BTT_DoActivitiesOfCurrentSpot.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "ImanolTest/Activities/ActivitySpot.h"
#include "ImanolTest/Characters/AIGuyCharacter.h"
#include "ImanolTest/Components/DailyManagementComponent.h"
#include "ImanolTest/Controllers/AIGuyController.h"

UBTT_DoActivitiesOfCurrentSpot::UBTT_DoActivitiesOfCurrentSpot()
{
	bNotifyTick = true;
	bNotifyTaskFinished = true;
	bCreateNodeInstance = true;
	NodeName = "Do Activities Of Current Spot";
}

EBTNodeResult::Type UBTT_DoActivitiesOfCurrentSpot::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AIGuyController = Cast<AAIGuyController>(OwnerComp.GetAIOwner());
	if(AIGuyController)
		ChosenActivitySpot = AIGuyController->GetActivitySpotFromBB();
	
	if(ChosenActivitySpot == nullptr)
		return EBTNodeResult::Failed;

	ChosenActivitySpot->ChooseAndStartActivity();
	return EBTNodeResult::InProgress;
}

void UBTT_DoActivitiesOfCurrentSpot::TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	if(ChosenActivitySpot == nullptr)
		FinishLatentTask(OwnerComp, EBTNodeResult::Failed);

	if(ChosenActivitySpot->IsActivitySpotCompleted())
		FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
}

void UBTT_DoActivitiesOfCurrentSpot::OnTaskFinished(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, EBTNodeResult::Type TaskResult)
{
	if(ChosenActivitySpot && AIGuyController)
	{
		ChosenActivitySpot->ExitCurrentActivitySpot();
		
		AAIGuyCharacter* aiGuyCharacter = AIGuyController->GetControlledAIGuy();
		if(aiGuyCharacter && aiGuyCharacter->GetDailyManagementComponent())
			aiGuyCharacter->GetDailyManagementComponent()->ResetCurrentSpotAndBT();
	}
}