#pragma once

#include "CoreMinimal.h"
#include "Subsystems/WorldSubsystem.h"
#include "AIGuysManager.generated.h"

class AAIGuyController;
class AAIGuyCharacter;
class AActivitySpot;
class UActivity;

UCLASS()
class IMANOLTEST_API UAIGuysManager : public UWorldSubsystem
{
	GENERATED_BODY()

	TArray<AAIGuyCharacter*> AIGuysCharacters;
	TArray<AActivitySpot*> ActivitySpots;
	TArray<AActivitySpot*> SleepingSpots;

public:
	virtual void OnWorldBeginPlay(UWorld& InWorld) override;

	/** Obtains an ActivitySpot that is not in the list. */
	UFUNCTION()
	AActivitySpot* ObtainRandomActivitySpot(bool assignSleepingSpot, const TArray<AActivitySpot*>& spotsVisited) const;
	
	UFUNCTION(BlueprintCallable)
	TArray<AAIGuyCharacter*>& GetAIGuysCharacters();

	UFUNCTION(BlueprintCallable)
	TArray<AActivitySpot*>& GetActivitySpots();

	UFUNCTION(BlueprintCallable)
	TArray<AActivitySpot*>& GetSleepingSpots();

private:
	void InitializeAIGuysCharacters(UWorld& InWorld);
	void InitializeActivitySpots(UWorld& InWorld);
	
};
