#include "AIGuysManager.h"

#include "ImanolTest/Activities/ActivitySpot.h"
#include "ImanolTest/Characters/AIGuyCharacter.h"
#include "Kismet/GameplayStatics.h"

void UAIGuysManager::OnWorldBeginPlay(UWorld& InWorld)
{
	Super::OnWorldBeginPlay(InWorld);

	InitializeAIGuysCharacters(InWorld);
	InitializeActivitySpots(InWorld);
}

AActivitySpot* UAIGuysManager::ObtainRandomActivitySpot(bool assignSleepingSpot, const TArray<AActivitySpot*>& spotsVisited) const
{
	int32 indexChecked = 0;
	TArray<AActivitySpot*> SpotsToLookFor;
	if(assignSleepingSpot)
		SpotsToLookFor = SleepingSpots;
	else
		SpotsToLookFor = ActivitySpots;
	
	for(int32 randomIndex = FMath::RandRange(0, SpotsToLookFor.Num() - 1); indexChecked < SpotsToLookFor.Num(); ++randomIndex)
	{
		if(randomIndex == SpotsToLookFor.Num())
			randomIndex = 0;

		AActivitySpot* currentActivitySpot = SpotsToLookFor[randomIndex];
		if(!currentActivitySpot->GetIsSpotOccupied() && !spotsVisited.Contains(currentActivitySpot))
			return currentActivitySpot;

		++indexChecked;
	}
	
	return nullptr;
}

TArray<AAIGuyCharacter*>& UAIGuysManager::GetAIGuysCharacters()
{
	return AIGuysCharacters;
}

TArray<AActivitySpot*>& UAIGuysManager::GetActivitySpots()
{
	return ActivitySpots;
}

TArray<AActivitySpot*>& UAIGuysManager::GetSleepingSpots()
{
	return SleepingSpots;
}

void UAIGuysManager::InitializeAIGuysCharacters(UWorld& InWorld)
{
	TArray<AActor*> actorsFound;
	UGameplayStatics::GetAllActorsOfClass(&InWorld, AAIGuyCharacter::StaticClass(), actorsFound);

	for(AActor* guyActor : actorsFound)
	{
		AAIGuyCharacter* guyCharacter = Cast<AAIGuyCharacter>(guyActor);
		if(guyCharacter)
			AIGuysCharacters.Add(guyCharacter);
	}
}

void UAIGuysManager::InitializeActivitySpots(UWorld& InWorld)
{
	TArray<AActor*> actorsFound;
	UGameplayStatics::GetAllActorsOfClass(&InWorld, AActivitySpot::StaticClass(), actorsFound);

	for(AActor* activitySpotActor : actorsFound)
	{
		AActivitySpot* activitySpot = Cast<AActivitySpot>(activitySpotActor);
		if(activitySpot)
		{
			if(activitySpot->GetIsSleepingSpot())
				SleepingSpots.Add(activitySpot);
			else
				ActivitySpots.Add(activitySpot);
		}
	}
}
