// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ImanolTestGameMode.generated.h"

UCLASS(minimalapi)
class AImanolTestGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AImanolTestGameMode();
};



