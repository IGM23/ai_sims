// Copyright Epic Games, Inc. All Rights Reserved.

#include "ImanolTestGameMode.h"
#include "ImanolTestCharacter.h"
#include "UObject/ConstructorHelpers.h"

AImanolTestGameMode::AImanolTestGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
