#include "AIGuyController.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "ImanolTest/Activities/ActivitySpot.h"
#include "ImanolTest/Characters/AIGuyCharacter.h"

void AAIGuyController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	AAIGuyCharacter* aiGuy = Cast<AAIGuyCharacter>(InPawn);
	if(aiGuy)
		ControlledAIGuy = aiGuy;

	if(BehaviorTree)
		RunBehaviorTree(BehaviorTree);
}

AAIGuyCharacter* AAIGuyController::GetControlledAIGuy()
{
	return ControlledAIGuy;
}

AActivitySpot* AAIGuyController::GetActivitySpotFromBB()
{
	return Cast<AActivitySpot>(GetBlackboardComponent()->GetValueAsObject("CurrentActivitySpot"));
}

FVector AAIGuyController::GetActivitySpotLocationFromBB()
{
	return GetBlackboardComponent()->GetValueAsVector("CurrentSpotLocation");
}

void AAIGuyController::SetActivitySpotAndLocationInBB(AActivitySpot* activitySpot)
{
	GetBlackboardComponent()->SetValueAsObject("CurrentActivitySpot", activitySpot);
	GetBlackboardComponent()->SetValueAsVector("CurrentSpotLocation", activitySpot->GetActorLocation());
}

void AAIGuyController::ClearActivitySpotAndLocationInBB()
{
	GetBlackboardComponent()->ClearValue("CurrentActivitySpot");
	GetBlackboardComponent()->ClearValue("CurrentSpotLocation");
}
