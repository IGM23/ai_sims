#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "AIGuyController.generated.h"

class AActivitySpot;
class AAIGuyCharacter;

UCLASS(Abstract, Blueprintable, BlueprintType)
class IMANOLTEST_API AAIGuyController : public AAIController
{
	GENERATED_BODY()

protected:
	UPROPERTY()
	AAIGuyCharacter* ControlledAIGuy = nullptr;

	UPROPERTY(EditAnywhere, Category = "AI Guys", meta = (AllowPrivateAccess = true))
	UBehaviorTree* BehaviorTree = nullptr;

public:
	virtual void OnPossess(APawn* InPawn) override;
	
	UFUNCTION(BlueprintCallable)
	AAIGuyCharacter* GetControlledAIGuy();

	UFUNCTION(BlueprintCallable)
	AActivitySpot* GetActivitySpotFromBB();

	UFUNCTION(BlueprintCallable)
	FVector GetActivitySpotLocationFromBB();

	void SetActivitySpotAndLocationInBB(AActivitySpot* activitySpot);
	void ClearActivitySpotAndLocationInBB();
};
