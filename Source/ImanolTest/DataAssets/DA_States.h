#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "DA_States.generated.h"

UENUM(BlueprintType, Blueprintable)
enum class EStates : uint8
{
	None		UMETA(DisplayName = "None"),
	Awake		UMETA(DisplayName = "Awake"),
	Sleeping	UMETA(DisplayName = "Sleeping"),
};


/** When we want to create a character with different states, create another DataAsset
 * which inherits from this class and fill 'AIGuysStateAndDurations' with the desired states. */
UCLASS(Abstract, BlueprintType, Blueprintable)
class IMANOLTEST_API UDA_States : public UDataAsset
{
	GENERATED_BODY()

public:
	/** AIGuy states and their durations. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AI Guys States")
	TMap<EStates, float> AIGuysStateAndDurations;
	
public:
	TArray<EStates> GetAllStates() const;

	float GetDurationOfState(EStates state) const;
};
