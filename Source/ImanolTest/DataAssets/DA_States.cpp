#include "DA_States.h"

TArray<EStates> UDA_States::GetAllStates() const
{
	TArray<EStates> allStates;
	AIGuysStateAndDurations.GetKeys(allStates);

	return allStates;
}

float UDA_States::GetDurationOfState(EStates state) const
{
	return *AIGuysStateAndDurations.Find(state);
}
