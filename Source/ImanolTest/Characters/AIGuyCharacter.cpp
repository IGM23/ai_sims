#include "AIGuyCharacter.h"

#include "ImanolTest/Components/DailyManagementComponent.h"

AAIGuyCharacter::AAIGuyCharacter()
{
	PrimaryActorTick.bCanEverTick = false;

	DailyManagementComponent = CreateDefaultSubobject<UDailyManagementComponent>(TEXT("Daily Management Component"));
}

UDailyManagementComponent* AAIGuyCharacter::GetDailyManagementComponent() const
{
	return DailyManagementComponent;
}

void AAIGuyCharacter::SetDailyManagementComponent(UDailyManagementComponent* managementCmp)
{
	DailyManagementComponent = managementCmp;
}

void AAIGuyCharacter::BeginPlay()
{
	Super::BeginPlay();
}

void AAIGuyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}
