#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AIGuyCharacter.generated.h"

class UDailyManagementComponent;

UCLASS(Abstract, Blueprintable, BlueprintType)
class IMANOLTEST_API AAIGuyCharacter : public ACharacter
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI Guys", meta = (AllowPrivateAccess = true))
	UDailyManagementComponent* DailyManagementComponent = nullptr;

public:
	AAIGuyCharacter();

	UFUNCTION(BlueprintCallable)
	UDailyManagementComponent* GetDailyManagementComponent() const;
	UFUNCTION(BlueprintCallable)
	void SetDailyManagementComponent(UDailyManagementComponent* managementCmp);

protected:
	virtual void BeginPlay() override;
	
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};
